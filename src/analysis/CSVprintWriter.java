package analysis;
import java.io.*;

public class CSVprintWriter {
	PrintWriter pw;
	
	// Constructor
	public CSVprintWriter(String fileName) {
		try {
			this.pw = new PrintWriter(fileName);
		} catch (FileNotFoundException fnex) {
			fnex.printStackTrace();
		}
	}
	
	public CSVprintWriter(File file) {
        try {
            this.pw = new PrintWriter(file);
        } catch (FileNotFoundException fnex) {
            fnex.printStackTrace();
        }
    }

    public void close() {
        this.pw.close();
    }

    public void print(Object arg) {
        synchronized (this.pw) {
            this.pw.print(arg);
            this.pw.print(",");
        }
    }

    public void println(Object... args) {
        synchronized (this.pw) {
            for (int i = 0; i < args.length; i++) {
                this.pw.print(args[i]);
                this.pw.print(",");
            }
            this.pw.println();
        }
    }

}
